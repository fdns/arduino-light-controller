int rele[] = {2,4,7,12};
int releN = 4;
int led = 13;
boolean ledStatus = false;

// Iniciacion del los puertos digitales
void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  for (int i = 0; i < releN; i++)
    pinMode(rele[i], OUTPUT);
}

// Secuencia - Enciende cada luz de forma unica y lineal
void continuous() {
  int d = 200;
  digitalWrite(rele[0],HIGH);
  digitalWrite(rele[3],LOW);
  delay(d);
  digitalWrite(rele[0],LOW);
  digitalWrite(rele[1],HIGH);
  delay(d);
  digitalWrite(rele[1],LOW);
  digitalWrite(rele[2],HIGH);
  delay(d);
  digitalWrite(rele[2],LOW);
  digitalWrite(rele[3],HIGH);
  delay(d);
}

// Secuencia - Intercambia las luces cada segundo
void clock() {
  int d = 1000;
  digitalWrite(rele[0],HIGH);
  digitalWrite(rele[1],HIGH);
  digitalWrite(rele[2],LOW);
  digitalWrite(rele[3],LOW);
  delay(d);
  digitalWrite(rele[0],LOW);
  digitalWrite(rele[1],LOW);
  digitalWrite(rele[2],HIGH);
  digitalWrite(rele[3],HIGH);
  delay (d);
}

// Secuencia - Enciende las luces de forma aleatoria
void randomLight() {
  int d = 250;
  digitalWrite(rele[0], (random(0,2) == 0) ? HIGH : LOW);
  digitalWrite(rele[1], (random(0,2) == 0) ? HIGH : LOW);
  digitalWrite(rele[2], (random(0,2) == 0) ? HIGH : LOW);
  digitalWrite(rele[3], (random(0,2) == 0) ? HIGH : LOW);
  delay(d);
}

// Secuencia - Enciende y apaga todas las luces cada 2 segundo
void allOnOff() {
  int d = 1000;
  digitalWrite(rele[0],HIGH);
  digitalWrite(rele[1],HIGH);
  digitalWrite(rele[2],HIGH);
  digitalWrite(rele[3],HIGH);
  delay(d);
  digitalWrite(rele[0],LOW);
  digitalWrite(rele[1],LOW);
  digitalWrite(rele[2],LOW);
  digitalWrite(rele[3],LOW);
  delay(d);
}

// Secuencia de pruebas - Enciende todas las luces
void allOn() {
  int d = 1000;
  digitalWrite(rele[0],HIGH);
  digitalWrite(rele[1],HIGH);
  digitalWrite(rele[2],HIGH);
  digitalWrite(rele[3],HIGH);
  delay(d);
}

// Secuencia de pruebas - Apaga todas las luces
void allOff() {
  int d = 1000;
  digitalWrite(rele[0],LOW);
  digitalWrite(rele[1],LOW);
  digitalWrite(rele[2],LOW);
  digitalWrite(rele[3],LOW);
  delay(d);
}

// Guarda la funcion guardada en este momento
void (*func)(void) = &continuous;

// Realiza el loop del arduino
void loop() {
  digitalWrite(led, !ledStatus ? HIGH : LOW);
  ledStatus = !ledStatus;
  func();
  
  if (Serial.available() > 0) {
    char value = Serial.read();
    switch (value) {
      case '1': func = &continuous; break;
      case '2': func = &clock; break;
      case '3': func = &randomLight; break;
      case '4': func = &allOnOff; break;
      case '5': func = &allOn; break;
      case '6': func = &allOff; break;
    }
  }
}
